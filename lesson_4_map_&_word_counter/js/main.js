"use strict";

/*
		    ---------------------  Д/З  -------------------
Напиши функцию map(fn, array), которая принимает на вход функцию и массив, и обрабатывает каждый элемент
массива этой функцией, возвращая новый массив.
Обрати внимание: функция не должна изменять переданный ей массив:
*/

var testArray = [1, 2, 3, 4, 5];

var newArray = map(square, testArray);

console.log(newArray);
console.log(testArray);

function square(x) {
    return x * x;
}

function map(fn, array) {
    var result = [];
    for (var i = 0; i < array.length; i++) {
        result.push(fn(array[i]));
    }
    return result;
}

/*
		    ---------------------  Д/З  -------------------
Подсчитать сколько раз встречается каждое слово в тексте
Используются объекты т.к. в них каждый ключ уникальный. Ключом будет выступать слово из текста
*/
var userStr;

do {
    userStr = prompt('Введите текст:')
} while (!userStr);

for (var key in toCountWords(userStr)) {
    console.log(key + ': ' + toCountWords(userStr)[key]);
}

function toCountWords(str) {
    var result = str.toLowerCase().replace(/[^\w\s]*/g, "").replace(/\s{2,}/g, " ").split(' ');
    var wordsList = {};
    for (var i = 0; i < result.length; ++i) {
        var key = result[i];
        if (wordsList[key] !== undefined) {
            ++wordsList[key];
        }
        else {
            wordsList[key] = 1;
        }
    }
    return wordsList;
}
