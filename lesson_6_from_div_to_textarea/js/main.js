"use strict";

/*
Создайте <div>, который при нажатии Ctrl+E превращается в <textarea>.
Изменения, внесенные в поле, можно сохранить обратно в <div> сочетанием клавиш Ctrl+S,
при этом <div> получит в виде HTML содержимое <textarea>.
Если нажать Esc, то <textarea> снова превращается в <div>, изменения не сохраняются
 */

var div = document.getElementById("for_textarea");
var textarea = document.createElement("textarea", {id: "my_textarea"});

textarea.setAttribute("autofocus", "autofocus");

document.addEventListener("keydown", toEdit);
document.addEventListener("keydown", toCancel)
document.addEventListener("keydown", toSave);
// textarea.addEventListener("keydown", toSave);

function toCancel(e) {
    if (e.keyCode === 27) {
        e.preventDefault();
        if (!div.parentElement) {
            textarea.parentElement.replaceChild(div, textarea);
        }
        textarea.value = "";
        div.innerHTML = "";
    }
}

function toSave(e) {
    if (e.keyCode === 83 && e.ctrlKey) {
        e.preventDefault();
        var text = textarea.value;
        textarea.parentElement.replaceChild(div, textarea);
        div.innerHTML = text;
    }
}

function toEdit(e) {
    if (e.keyCode === 69 && e.ctrlKey) {
        e.preventDefault();
        div.parentElement.replaceChild(textarea, div);
    }
}
