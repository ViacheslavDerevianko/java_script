"use strict";

/*
Написать функцию, которая принимает число от 0 до 10000 и сообщает, сколько в нем тысяч, сотен, десятков и единиц.
 */

var num = prompt("Input number: ")

getDischarge(num);

function getDischarge(num) {
    if (!isNumeric(num)) {
        console.log("Argument is not a number.");
        return;
    }
    if (num < 0 || num > 10000) {
        console.log("Invalid argument.");
        return;
    }
    console.log("Thousands: " + Math.floor(num / 1000));
    num = num % 1000;
    console.log("Hundreds: " + Math.floor(num / 100));
    num = num % 100;
    console.log("Dozens: " + Math.floor(num / 10));
    num = num % 10;
    console.log("Units: " + Math.floor(num));
}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}
