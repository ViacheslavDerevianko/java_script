"use strict";

/*
Написать функцию, которая принимает год и сообщает високосный он или нет. Валидация данных необходима.
 */

var year = prompt("Input year: ")

console.log(isLeapYear(year));

function isLeapYear(year) {
    if (!isNumeric(year)) {
        return "Argument is not a number.";
    }
    if (year <= 0) {
        return "Invalid argument.";
    }
    year = Math.floor(year);
    var date = new Date(year, 2, 0);
    if (date.getDate() === 29) {
        return "Year is leap.";
    }
    return "Year is not leap.";
}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}
