"use strict";

/*
Написать функцию filter, которая принимает массив и функцию-фильтр.
Результат работы - новый массив, полученный путем применения функции-фильтра к каждому элементу исходного массива.
Функция - фильтр принимает один аргумент и возвращает  true или false.
Все массивы в задачах должны быть сгенерированы из случайных чисел.
 */

var testArray = generateRandomArray(0, 100, 100);

console.log(testArray);
console.log(filter(testArray, isEven));

function filter(arr, fn) {
    var result = [];

    arr.forEach(function (item) {
        if (fn(item)) {
            result.push(item);
        }
    });
    return result;
}

function isEven(x) {

    if (x % 2 === 0) {
        return true;
    }
    return false;
}

function getRandomNumber(min, max) {

    return Math.floor(Math.random() * (max - min + 1) + min);
}

function generateRandomArray(min, max, quantity) {

    var result = [];

    for (var i = 0; i < quantity; i++) {
        result[i] = getRandomNumber(min, max);
    }

    return result;
}
