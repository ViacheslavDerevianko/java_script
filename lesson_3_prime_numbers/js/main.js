"use strict";

/*
            ---------------------  Д/З  -------------------
Вывести простые числа из диапазона (простое число: > 1, делится только на себя и 1)
*/

var min, max;

// ------------ Запрашиваем первое число --------------
do {
    min = parseInt(prompt("Введите целое число больше единицы"), 10);
} while (min < 2 || isNaN(min));

// ------------ Запрашиваем второе число --------------
do {
    max = parseInt(prompt("Введите второе целое число больше первого числа"), 10);
} while (max < min || isNaN(max));

showPrimes(min, max);

// функция проверяет является ли число простым
function isPrime(n) {
    if (n <= 1) {
        return false;
    }
    for (var d = 2; d * d <= n; d++) { // перебираем возможные делители от 2 до sqrt(n)
        if (n % d === 0)
            return false;
    }
    return true;
}

// функция возвращает массив простых чисел из диапазона
function getPrimes(min, max) {
    var arr = [];
    for (var i = min; i < max; i++) {
        if (isPrime(i)) {
            arr.push(i);
        }
    }
    return arr;
}

//функция выводит простые числа из диапазона
function showPrimes(min, max) {
    console.log(getPrimes(min, max));
}